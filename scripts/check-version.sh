#!/usr/bin/env bash

LATEST_VERSION=$(yarn info . --json | jq -r '.data.version')
CURRENT_VERSION=$(cat package.json | jq -r .version)

print_red() { printf "\\033[0;31m%s\\033[0m\\n" "$1"; }
print_green() { printf "\\033[1;32m%s\\033[0m\\n" "$1"; }

print_error() {
  local message=$1
  local exitcode=$2
  print_red "==========================================================================="
  print_red "$message"
  print_red "==========================================================================="
  [ -n "$exitcode" ] && exit "$exitcode" || true
}

if [[ ${LATEST_VERSION} == ${CURRENT_VERSION} ]]; then
  print_error "ERROR: ${CURRENT_VERSION} already published" 1
else
  print_green "Allow to publish ${CURRENT_VERSION} version"
fi
